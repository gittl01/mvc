<?php

require_once('init.php');
class banco{
        protected $mysqli;

        public function __construct()
        {
            echo "Conexão efetuada com sucesso";
            $this->conexao();
        }

        private function conexao(){
            $this->mysqli = new mysqli(HOST, USUARIO, SENHA, DB);
        }
        public function setLivro($nome, $autor, $quantidade, $preco, $data){
        $stmt = $this->mysqli->prepare("INSERT INTO livros (`nome`, `autor`, `quantidade`, `preco`, `data`) VALUES (?,?,?,?,?)");

        $stmt->bind_param("sssss", $nome, $autor, $quantidade, $preco, $data);
        if ($stmt->execute() == TRUE) {
            return true;
        } else {
            return false;
        }
    }
}
?>